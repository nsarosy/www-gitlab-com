---
layout: markdown_page
title: "Category Vision - Error Tracking"
---

- TOC
{:toc}

## Error Tracking

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thanks for visiting this category strategy page on Error Tracking in GitLab. This category belongs to the [Health](/handbook/engineering/development/ops/monitor/health/) group of the Monitor stage and is maintained by Sarah Waldner ([swaldner@gitlab.com](mailto:<swaldner@gitlab.com>)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AError%20Tracking) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AError%20Tracking) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for Error Tracking, we'd especially love to hear from you.

### Overview
Error Tracking is the process of proactively identifying application errors and fixing them as quickly as possible. At GitLab, this functionality is based on an integration with [Sentry](https://sentry.io/welcome/) which aggregates errors found by Sentry, surfaces them in the GitLab UI, and provides the tools to triage and respond to the critical ones. GitLab leverages Sentry's intelligence to provide pertinent information such as the user impact or commit that caused the bug. Throughout the triage process, Users have the option of creating GitLab issues on critical errors to track the work required to fix them, all without leaving GitLab. Our mission is to reduce time spent fixing errors by enabling triage, response, and resolution in a single tool: GitLab.

#### Target audience
The primary persona for Error Tracking is the [Software Developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer). Tracking and triaging errors is a part of any release process and the individual most qualified to solve the error is the developer who wrote the code. Secondarily, we are building Error Tracking for [DevOps Engineers](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer) who leverage it while investigating larger scale incidents or outages.

### Challenges to address
The following list of Jobs to be Done is an aggregation of the most common responsibilities we identified for Software Developers surrounding error management and response. These are the workflows we aim to streamline and improve. Please check out the [research](https://gitlab.com/gitlab-org/ux-research/issues/366) which validates these findings.

* Respond to customer support tickets
* Manually investigate error alerts
* Resolve errors in code
* Communicate status of problems to stakeholders

### Where we are headed
When our vision for Error Tracking is fully realized, Developers will know about errors before their customers report them - every time. Following a release or during daily triage activities, GitLab will it make simple to identify new errors and the customer impact. By intuitively surfacing errors on Merge Requests or Releases, we can ensure responsible individuals have an aggregated view of errors resulting from a deployment. GitLab's ownership of the development workflow makes it simple to remove additional interfaces from the tool chain which will decrease time spent and increase throughput:

* Triaging errors - Errors live in the same place as code and are immediately correlated to the commit or merge request that caused it
* Investigating errors - The stack trace is embedded in GitLab issues and users can drill into the specific line of code via hyperlinks
* Tracking work - Close the GitLab issue once a fix is verified to communicate with stakeholders that services have been restored

#### What's Next & Why
Our plan is to provide a streamlined triage experience where developers can easily identify new, important errors directly within GitLab and quickly roll out a fix. To mature Error Tracking from minimal to viable, we will be focusing on:

* Creating a [detailed view of individual errors in GitLab](https://gitlab.com/gitlab-org/gitlab/issues/32464)
* Adding [filtering and sorting capabilities to the error list view](https://gitlab.com/groups/gitlab-org/-/epics/2031)
* Correlating errors with [releases](https://gitlab.com/gitlab-org/gitlab/issues/33884), [merge requests](https://gitlab.com/gitlab-org/gitlab/issues/33886), and [commits](https://gitlab.com/gitlab-org/gitlab/issues/33885)
* [Opening GitLab issues from errors](https://gitlab.com/gitlab-org/gitlab/issues/33156)
* Making it quick to get started on Sentry by [deploying it to the cluster](https://gitlab.com/gitlab-org/gitlab/issues/26513) your application is running on in a few clicks
* [Automatically reporting releases to Sentry](https://gitlab.com/gitlab-org/gitlab/issues/26028) to reduce manual, repetitive tasks

#### What is not planned right now
For the immediate future, GitLab's Error Tracking offering will be based on an integration with [Sentry](https://sentry.io/welcome/). We will not build out a separate error monitoring agent.

#### Maturity Plan
This category is currently at the `minimal` maturity level, and our next maturity target is `viable` (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). All work scoped for `viable` is captured in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/1625).

### User Success Metrics
We will know we are on the right trajectory for Error Tracking when we are able to observe the following:
* Broad adoption across internal engineering teams (i.e. we are [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding)) of a [centralized sentry instance](https://sentry.gitlab.net/).
* Increase in instances/projects with [Sentry Error Tracking enabled](https://docs.gitlab.com/ee/user/project/operations/error_tracking.html#enabling-sentry)
* Increase in instances [generating issues from Sentry errors](https://gitlab.com/gitlab-org/gitlab/issues/33847)
* Increase in the number of [clusters with Sentry deployed](https://gitlab.com/gitlab-org/gitlab/issues/26513)

### Why is this important?
At a 30,000 ft view, adding Error Tracking to GitLab drives the Single app for the DevOps lifecycle vision and works to speed up the broader DevOps workflow. We can eliminate an additional interface from the multitude of tools Developers are required to use each day to do their jobs. Furthermore, GitLab has the opportunity to surface errors early and often in the development process. By surfacing errors caused by a particular commit, merge request, or release, we can easily prevent our user's customers from experiencing the bug at all.

We are confident in our execution of this vision as Sentry already provides correlation between errors and commits by identifying [suspect commits](https://docs.sentry.io/workflow/releases/?platform=browser#after-associating-commits). In just a few milestones, we will be able to take advantage of this intelligence and extend it within the GitLab interface by correlating errors and merge requests and releases.

## Competitive Landscape

* [Raygun](https://raygun.com/)
* [Rollbar](https://www.google.com/aclk?sa=l&ai=DChcSEwjWtYX5qbjlAhUH22QKHVlqBoUYABAAGgJwag&sig=AOD64_0vLeLukSK6oYi8AuSM04Scz29t3Q&q=&ved=2ahUKEwijkP_4qbjlAhWJqp4KHVQeClQQ0Qx6BAgIEAE&adurl=)
* [Airbrake](https://airbrake.io/)