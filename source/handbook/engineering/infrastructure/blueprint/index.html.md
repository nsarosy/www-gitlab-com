---
layout: handbook-page-toc
title: "Blueprint"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

**Blueprint** content has been moved to the [**Library**](../library/).
